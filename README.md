# About 
This repo was created for help me and you to prepare for job interview Django developer.
This roadmap will prepare you as a PROGRAMMER, not just coder :)
So, it is why here you can find information and material from almost all fields of computer science.
Let's begin!!!


- Operating systems
We will start from operating systems, why? Because you use it every day and not only for work. You watch videos, play games, create folders, and so on. It is like your main environment, you have to know how easily create folders, documents, manipulate them, you have to know how to install something ... and your choice is definitely Linux. No matter what distribution, but it must be Linux. 

Basic commands:
ls -> show all documents in folder 
ls -la -> 
cd .. -> 
mkdir 
rmdir 
rm -r -> remove reccurent(cant delete not empty folders)



- Python
Virtula Env (python -m venv vname)
correct instalation (pip install django==3.2.*(no mattter minor version(last number)))
1. Zen of Python 
2. PEP8
3. Clean code, KISS, DRY
4. Generators, iterators 
5. Asyncronous 
6. Memory management, garbage collector 
7. zip
8. regex, 
9. decorators, 
10. filter map reduce, 
11. try except, 
12. OOP, __init__, super
13. *args, **kwargs 
14. Files
15. List/Dict comprehension 
16. collection library
17. TypeHint(Annotation)
18. Walrus operator
- Django 

1. installation 
APPS(startapp, startproject), SETTINGS(.env), 
makemigrations, migrate, createsuperuser 
1. CRUD
CREATE
READ 
UPDATE
DELETE
2. Authentication and authorization
Authentication - who user is. Check if user exist in system(database).
`from django.contrib.auth import authenticate
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        ...
    else:
        # Return an 'invalid login' error message.` 

Authorization - what user can do and what user can see on the system.
Django session uses cookies

`from django.contrib.auth.decorators import login_required`
@login_required(login_url='login') -> decorator for views

- Token Authentication and Authorization(JWT) 
Where to store token? (http only cookies, localstorage)
Access token and refresh token 
XSS and CSRF attacks 
3. Forms 
4. CORS
5. Middlewares 
6. Templates
7. 

- Django REST framework
1. Serialization(JSON)

- Databases
1. SQL 
SQL vs NOSQL??
A: No relation, horizontal scalable, key value pairs
PostgreSQL vs SQLite??
2. Normalization
delete duplication
3. JOINS, WINDOW FUNTIONS, UNION
- Networks 
http, https, requests, responses, cookies, headers


- other stuff
1. Git 
2. Docker
# Freequently asked questions 
1. Difference between app and project and how to start them 
A: django-admin startproject projectname 
django-admin startapp appname 
Project it is configuration 
Apps are components of your site
Apps are parts of project like puzzle 
Example: project Facebook
Apps: Users(login, verification), news feed, filter bar
Start development server: python manage.py runserver 
Start tests: python manage.py test appname
2. MVC MVT
A: Django is actually MVT (model view template)
MVC(model view controller)
3. makemigrations vs migrate 
makemigrations -> it is like history of migrations 
migrate -> forces changes to database 
4. CBS vs FBS 
CBS(class based views)
FBS(function based views)
Class based views helps us build code with DRY method 
5. settings.py 
A: it is necessary to create .env, were we store database data, SECRET_KEY and other things
6. Templates, inheriting, static ...
{% extends "base.html" %} {% load static %} {% include "base.html" %}
store them in templates/appname
7. Static files Where to store? ...
static/css or js or images in app folder 
collectstatic 
STATIC_ROOT, STATICFILES_DIRS in settings 
MEDIA_ROOT - user generate files(uploading)
ulr 
whitenoise -> to serve static in production 
8. Models
models.ForeignKey(User, on_delete=models.CASCADE) -> one to many relationship on_delete(if we delete object what do we do with relation)
9. Queries 
Get all object from database -> Model.objects.all()
Get one(only) object from database -> Model.objects.get(params) returns object 
Get multiple or filter obects from db -> Model.object.filter(params) returns QueyDict even if 0 objects returns

attr__lt, attr__gt -> less then greater than -> filter(id__lte=14)..
attr__contains, attr__exact -> contains template, exact template (name__exact="Paul")...

Q expression -> filtering with multiple same parametrs
from django.db.models import Q
filter(Q(age__lt=60) | Q(age_gt=18)) 
| or & and 

F expression  -> compare model field with field of the same model 
Entry.objects.filter(authors__name=F('blog__name'))
Entry.objects.filter(number_of_comments__gt=F('number_of_pingbacks'))
10. CSRF token 
use it in forms when we use POST method 
11. Forms 
class based representation of models
Example: User registration form 
12. Views
@login_required decorator 
13. Signals 
14. Serialization
Python data turning into JSON, XML .. 
15. Authentication, sessions 
JWT(JSON Web Token)
16. Tests 
from django.test import TestCase
class TaskModelTests(TestCase):
def test_smthing(self):
self.assertIs(task.status, Task.ACTIVE)
17. Viewsets, Routers

# Projects Example 
1. E-commerce 

Фабрику какую нибудь. 
Q, F объекты. 
запросы к связанным моделям чрез select и perfetch related. 
Общую оптимизацию запросов.
Если еще и индексы будут - вообще шикарно. 
В пользователях -- сигналы показать, например.
И будет хорошо.Нужен проект сильно сложнее того, что есть в официальном туориале.
Основная посылка -- я должен посмотерть и понять, что человек может тянуть те задачи, которые в реальности прилетают. И даже сложнее.

# Books 
1. two scoops of django 4
# Good Links
- https://www.tivix.com/blog/django-scaffolding
